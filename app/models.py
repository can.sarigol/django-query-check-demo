from django.db import models
from query_check.base.models import BaseModel

class CheckModel(models.Model):
    name = models.CharField(max_length=100)


class TestModel(BaseModel):
    fk_column = models.ForeignKey(CheckModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
